#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

import json
import cgi
import re
import botconfig
import time

urlPattern = re.compile(r"(^|[\n ])(([\w]+?://[\w\#$%&~.\-;:=,?@\[\]+]*)(/[\w\#$%&~/.\-;:=,?@\[\]+]*)?)", re.IGNORECASE | re.DOTALL)

html_header = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>LearnDB HTML version</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
<div class=\"pagetitle\">
<h1>{0} learn database</h1>
<h2>updated {1}. {2} items ({3} entries).</h2>
</div>
"""
html_footer = """
</body>
</html>
"""

section = "<h1>{0}</h1>\n"
entry = "\n<h2 id=\"{0}\"><a class=\"none\" href=\"#{0}\">{0}</a></h2>\n"
subentry = "<a class=\"none\" href=\"#{0}_{1}\"><em id=\"{0}_{1}\">{0}[{1}/{2}]</em>:</a> "
entryText = "<p>{0}</p>\n"
entryTextSingle = '<p class="single">{0}</p>\n'
linkToItem = '<a href="#{1}">{0}</a>'
urlstr = r'\1<a href="\2" target="_blank">\2</a>'

def idFromRel(item):
    try:
        rel = item.split('see ')[1]
        if rel.find('[') == -1:
            return rel
        num = rel.split('[')[1].split(']')[0]
        name = rel.split('[')[0]
        return name + '_' + num if int(num) > 1 else name
    except:
        return "invalid_link"

def processItemText(item):
    item = cgi.escape(item)
    if item.startswith('see ') and len(item) > 4:
        item = linkToItem.format(item, idFromRel(item))
    item = urlPattern.sub(urlstr, item)
    return unicode(item).encode('utf-8')

class HTMLExporter():
    def __init__(self, dbfile, sectioned=False):
        self.sectioned = sectioned
        with open(dbfile, 'r') as f:
            self.db = json.load(f)

    def export(self, toFile):
        output = open(toFile, 'w')
        output.write(html_header.format(botconfig.channel, time.strftime("%a %d %b %I:%M:%S%p"),
                                        len(self.db), sum([len(item) for item in self.db.values()])))
        keys = sorted(self.db.keys())
        firstLetter = ''
        for key in keys:
            if key[0] != firstLetter and self.sectioned:
                firstLetter = key[0]
                output.write(section.format(firstLetter))
            output.write(entry.format(key.replace('_', ' ')))
            if len(self.db[key]) > 1:
                for index, item in enumerate(self.db[key]):
                    item = processItemText(item)
                    paragraph = subentry.format(key, index+1, len(self.db[key]))
                    output.write(entryText.format(paragraph + item))
            else:
                output.write(entryTextSingle.format(processItemText(self.db[key][0])))
        output.write(html_footer)
        output.close()

if __name__ == '__main__':
    h = HTMLExporter("test.learndb")
    h.export("test.html")