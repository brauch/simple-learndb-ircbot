#!/usr/bin/env python2.7

import socket
import time
import errno
from os import path
import json
import shutil
from threading import Timer
import re

import botconfig

def getCommand(line):
    d = line.split(' ')
    try:
        return int(d[1])
    except:
        return -1

def getEvent(line):
    d = line.split(' ')
    try:
        return d[1]
    except:
        return ""

class Connection():
    def __init__(self, host, port, channel, nick):
        self.channel = channel
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

        self.send("USER {0} 0 0 :{0}".format(nick))
        self.send("NICK {0}".format(nick))
        self.waitForCommand(376) # end of MOTD
        self.send("JOIN {0}".format(channel))
        self.waitForCommand(366) # end of NAMES list

    def send(self, command):
        print " >", command
        self.socket.send(command + "\n")

    def waitForEvent(self, event):
        data = ""
        while getEvent(data) != event:
            data = self.recv()

    def waitForCommand(self, command):
        data = ""
        while command not in [getCommand(line) for line in data.splitlines()]:
            print getCommand(data)
            data = self.recv()

    def recv(self):
        add = "empty"
        data = ""
        while len(add) > 0 and add[-1] != "\n":
            try:
                add = self.socket.recv(1024)
            except socket.error as e:
                if e.errno == errno.EAGAIN:
                    time.sleep(0.01)
                    continue
            data += add
        print " <", data,
        return data

    def message(self, message):
        self.send("PRIVMSG {0} :{1}".format(self.channel, message))

class PrivMsg():
    def __init__(self, string):
        d = string.split(' ')
        if len(d) < 4:
            return
        self.sender = d[0]
        self.channel = d[2]
        self.message = ' '.join(d[3:])[1:]

    def __repr__(self):
        return "<Message from {0} to channel {1}: '{2}'>".format(self.sender, self.channel, self.message)

class BasicBot():
    def __init__(self, connection):
        if False: self.connection = Connection() # kdeveop
        self.connection = connection
        self.pingReceived()

    def pingReceived(self):
        self.lastPing = time.time()

    def messageKind(self, message):
        if message.startswith('PING'):
            return 'PING'
        elif message.startswith('ERROR'):
            return 'ERROR'
        else:
            return "normal"

    def messageType(self, message):
        try:
            return message.split(' ')[1]
        except:
            return "invalid"

    def handleMessages(self, messages):
        for message in messages:
            self.handleMessage(message)

    def handleMessage(self, message):
        self.pingReceived()
        if self.messageKind(message) == "PING":
            self.connection.send("PONG")
        elif self.messageType(message) == "PRIVMSG":
            self.handlePrivateMessage(PrivMsg(message))

    def handlePrivateMessage(self, msg):
        pass

    def mainloop(self):
        while True:
            messages = self.connection.recv().splitlines()
            self.handleMessages(messages)

""" data structure:
{
    "entry name": [entry 1, entry 2],
    "entry name 2": [foo, bar, baz, bang]
}
"""
class LearnDb():
    def __init__(self, dbfile):
        self.dbfile = dbfile
        self.logfile = dbfile + ".transaction_log"
        if path.exists(dbfile):
            try:
                self.loadFrom(dbfile)
            except:
                self.data = dict()
        else:
            self.data = dict()

    def loadFrom(self, dbfile):
        with open(dbfile, 'r') as f:
            self.data = json.load(f)

    def saveToDisk(self):
        # TODO cycling backup
        try:
            shutil.copy(self.dbfile, self.dbfile + ".old")
        except IOError:
            print "warn: failed to backup {0}".format(self.dbfile)
        with open(self.dbfile, 'w') as f:
            json.dump(self.data, f)
        try:
            import export
            exporter = export.HTMLExporter(self.dbfile)
            exporter.export(self.dbfile + ".html")
        except:
            pass

    def syntax(self):
        return "Syntax is: !learn {add,del,swap,replace,edit,insert} <arguments>. See !learn <command> for more info."

    def addEntry(self, name, value):
        if name in self.data:
            self.data[name].append(value)
        else:
            self.data[name] = [value]
        self.saveToDisk()
        return self.getEntry(name, len(self.data[name]) - 1)

    def getEntry(self, name, index, allowRedirect=True):
        if name in self.data and index < len(self.data[name]) and index >= 0:
            entry = self.data[name][index]
            if entry.startswith('see') and allowRedirect:
                s = entry.split(' ')
                if len(s) == 2:
                    newEntry, newIndex = self.parseBrackets(s[1])
                    if self.isValidEntry(newEntry, newIndex):
                        return self.getEntry(newEntry, newIndex, False)
            return "{0}[{1}/{2}]: {3}".format(name.replace('_', ' '), index + 1, len(self.data[name]),
                                              entry)
        else:
            return "No such thing."

    def isValidEntry(self, entry, index):
        return entry in self.data and index < len(self.data[entry])

    def deleteEntry(self, entry, index):
        if self.isValidEntry(entry, index):
            old = self.data[entry][index]
            del self.data[entry][index]
            if len(self.data[entry]) == 0:
                del self.data[entry]
            self.saveToDisk()
            return "Deleted {0}[{1}] ({2}).".format(entry, index + 1, old)
        else:
            return "That doesn't even exist."

    def swapEntries(self, entry, index1, index2):
        if self.isValidEntry(entry, index1) and self.isValidEntry(entry, index2):
            self.data[entry][index1], self.data[entry][index2] = self.data[entry][index2], self.data[entry][index1]
            self.saveToDisk()
            return "Swapped entries {0} and {1} of {2}.".format(index1, index2, entry)
        else:
            return "Can't swap those, one of them doesn't exist."

    def insertEntry(self, entry, index, text):
        if entry not in self.data:
            return self.addEntry(entry, text)
        elif index < 0:
            return "Invalid index '{0}'.".format(index)
        else:
            self.data[entry].insert(index, text)
            self.saveToDisk()
            return self.getEntry(entry, index)

    def parseRegex(self, regex):
        if regex.count('/') < 3:
            raise ValueError("Invalid delimiters.")
        if not regex.startswith('s'):
            raise ValueError("Unsupported operation '{0}'.".format(regex[0]))
        stage = 0
        i = 0
        op, expression, replace, opts = [str()]*4
        while i < len(regex):
            if regex[i] == '/':
                i += 1
                stage += 1
                continue
            if regex[i] == '\\':
                i += 1
            if i == len(regex):
                break

            if stage == 0:
                op += regex[i]
            elif stage == 1:
                expression += regex[i]
            elif stage == 2:
                replace += regex[i]
            elif stage == 3:
                opts += regex[i]
            else:
                raise ValueError("Too many delimiters")
            i += 1

        return op, expression, replace, opts

    def regexEdit(self, entry, index, regex):
        if not self.isValidEntry(entry, index):
            return "No such entry {0}[{1}].".format(entry, index)
        try:
            op, regex, replace, opts = self.parseRegex(regex)
        except ValueError as e:
            return str(e)
        count = 0 if 'g' in opts else 1 # greedy replace
        oldEntry = self.data[entry][index]
        try:
            self.data[entry][index] = re.sub(regex, replace, oldEntry,
                                            count, re.IGNORECASE if 'i' in opts else 0)
        except Exception as e:
            return "Failed to apply regular expression: '{0}'".format(str(e))
        if oldEntry == self.data[entry][index]:
            return "{0}[{1}] is unchanged -- maybe regex failed to match?".format(entry, index + 1)
        self.saveToDisk()
        return self.getEntry(entry, index)

    def isValidName(self, name):
        return name.replace('_', '').replace('-', '').isalnum()

    def saveLogEntry(self, query):
        with open(self.logfile, 'a+') as f:
            f.write("{0} {1}\n".format(time.time(), query))

    def query(self, query):
        q = query.split(' ')
        self.saveLogEntry(query)
        if len(q) < 2:
            return self.syntax()

        command = q[0]
        if command == "add":
            if len(q) < 3:
                return "Syntax: !learn add entry_name <some text>"
            entry = q[1]
            if not entry.replace('_', '').isalnum():
                return "'{0}' is not a valid entry name to add to".format(entry)
            return self.addEntry(entry, ' '.join(q[2:]))

        elif command == "del":
            if len(q) != 2:
                return "Syntax: !learn del entry_name[index] or !learn del entry_name (to delete entry_name[1])"
            entry, index = self.parseBrackets(q[1])
            if index == -1:
                return entry
            return self.deleteEntry(entry, index)

        elif command == "swap":
            if len(q) != 4:
                return "Syntax: !learn swap entry_name index1 index2"
            name = q[1]
            if not self.isValidName(name):
                return "Invalid name '{0}'.".format(name)
            try:
                index1 = int(q[2])
                index2 = int(q[3])
            except ValueError:
                return "Invalid indices provided"
            return self.swapEntries(name, index1 - 1, index2 - 1)

        elif command == "replace":
            if len(q) < 3:
                return "Syntax: !learn replace entry_name[index] <new text>"
            entry, index = self.parseBrackets(q[1])
            if index == -1:
                return entry
            text = ' '.join(q[2:])
            self.deleteEntry(entry, index)
            return self.insertEntry(entry, index, text)

        elif command == "insert":
            if len(q) < 3:
                return "Syntax: !learn insert entry_name[before_index] <new text>"
            entry, index = self.parseBrackets(q[1])
            if index == -1:
                return entry
            return self.insertEntry(entry, index, ' '.join(q[2:]))

        elif command == "edit":
            if len(q) < 3:
                return "Syntax: !learn edit entry_name[index] s/regex/replace/opts"
            entry, index = self.parseBrackets(q[1])
            if index == -1:
                return entry
            return self.regexEdit(entry, index, ' '.join(q[2:]))

        else:
            return "Unknown command '{0}'.".format(command)

    def parseBrackets(self, entry):
        e = entry.split('[')
        if len(e) > 2:
            return "Invalid lookup: '{0}'.".format(entry), -1
        index = 0
        if len(e) == 2:
            try:
                index = int(e[1].split(']')[0]) - 1
            except:
                return "Invalid entry index.", -1
        name = e[0]
        if not self.isValidName(name):
            return "Invalid entry name: '{0}'.".format(name), -1
        return e[0], index

    def lookup(self, query):
        name, index = self.parseBrackets(query)
        if index < 0:
            # an error in parsing the entry
            return name
        return self.getEntry(name, index)

class LearningBot(BasicBot):
    def __init__(self, *args, **kwargs):
        BasicBot.__init__(self, *args, **kwargs)
        self.learndb = LearnDb("test.learndb")

    def handlePrivateMessage(self, msg):
        if False: msg = PrivMsg() # kdevelop
        reply = None
        try:
            if msg.message.startswith('!learn'):
                try:
                    query = ' '.join(msg.message.split(' ')[1:])
                except IndexError:
                    query = str()
                reply = self.learndb.query(query)

            if msg.message.startswith('??'):
                try:
                    word = msg.message[2:].replace(' ', '_')
                except IndexError:
                    word = str()
                reply = self.learndb.lookup(word)

            if reply != None:
                self.connection.message(reply)
        except Exception as e:
            self.connection.message("Internal error: '{0}'. Sorry.".format(str(e)))


if __name__ == '__main__':
    c = Connection(botconfig.servername, botconfig.serverport, botconfig.channel, botconfig.nickname)
    bot = LearningBot(c)
    bot.mainloop()

